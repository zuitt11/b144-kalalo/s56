import { Fragment, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import WishlistCard from '../components/WishlistCard'

export default function Wishlist() {
	const [wishlist, setWishlist] = useState([]) 
	const { userId } = useParams()


	useEffect(()=>{
		fetch(`https://tranquil-journey-24938.herokuapp.com/api/users/wishlist/${userId}` , {
		headers: {
        	'Content-Type': 'application/json',
        	'Accept': 'application/json',
			Authorization:`Bearer ${ localStorage.getItem( 'token' ) }`
		}
	})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			setWishlist(
				data.map(wishlist =>{
						return(
							<WishlistCard key={wishlist._id} wishlistProp={wishlist} />
						);
					})
			)
		})
	}, [] );

	return(
		<Container>
			<h1>My Wishlist</h1>
			<Fragment>
				{wishlist}
			</Fragment>
		</Container>
	)
}