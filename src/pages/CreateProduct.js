import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Redirect, useHistory, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CreateProduct(){

	const {user} = useContext(UserContext);
	console.log(user)
	const history = useHistory();



	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [isActive, setIsActive] = useState(false);


	function createProduct(e) {
		e.preventDefault();
		fetch('https://tranquil-journey-24938.herokuapp.com/api/products/create', {
			method: "POST",
			headers: {
			    'Content-Type': 'application/json',
			    'Accept': 'application/json',
				Authorization:`Bearer ${ localStorage.getItem( 'token' ) }`
			},
			body: JSON.stringify({
			    name: name,
			    description: description,
			    price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {

			    setName('');
			    setDescription('');
			    setPrice('');

			    Swal.fire({
			        title: 'Product creation successful',
			        icon: 'success',
			        text: 'See Products page'
			    });


			} else {

			    Swal.fire({
			        title: 'Product already exists',
			        icon: 'error',
			        text: 'Please enter a new product.'   
			    });

			}
		})
			    history.push("/products");
	}
	useEffect(() => {
		if(name !== '' && description !== '' && price !== Number ){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [name, description, price])

	return (
		<Container>
			<h1>Create Product</h1>
			<Form className="mt-3" onSubmit={(e) => createProduct(e)}>
				<Form.Group controlId="name">
				    <Form.Label>Product Name</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter product name"
				        value={name} 
				        onChange={e => setName(e.target.value)}
				        required
				    />
				</Form.Group>
				<Form.Group className="mb-3" controlId="description">
			    <Form.Label>Description</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter product description" 
			    	value = {description}
			    	onChange = { e => setDescription(e.target.value)}
			    	required 
			    />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="price">
			    <Form.Label>Price</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder="Enter product price" 
			    	value={price}
			    	onChange = { e => setPrice(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			  { isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Create
			  		</Button>
			  	:
				  	<Button variant="primary" type="submit" id="submitBtn" disabled>
				  	  Create
				  	</Button>
			  }
			</Form>
		</Container>
	)
}
