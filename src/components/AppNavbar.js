import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import UserContext from '../UserContext';
import { Link, NavLink } from 'react-router-dom';
import  { useState, useContext, Fragment } from 'react';


export default function AppNavBar() {

	const {user} = useContext(UserContext);

	return(
		<Navbar bg="primary" expand="lg" >
		  <Container fluid className="mx-5">
		    <Navbar.Brand as={Link} to="/">BS</Navbar.Brand>
		    <Navbar.Toggle aria-controls="navbarScroll" />
		    <Navbar.Collapse id="navbarScroll">
		      <Nav
		        className="me-auto my-2 my-lg-0"
		        style={{ maxHeight: '100px' }}
		        navbarScroll
		      >
		        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
		        <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
		        {
	        		user.id !== null && user.isAdmin !== true ?
		        	<Fragment>
		        	<Nav.Link as={NavLink} to="/wishlist" exact >Wishlist</Nav.Link>
		        	<Nav.Link as={NavLink} to="/:userId/cart" exact>Cart</Nav.Link>
		        	</Fragment>
		        	:
		        	<Fragment>
		        	<Nav.Link as={NavLink} to="/wishlist" exact disabled>Wishlist</Nav.Link>
		        	<Nav.Link as={NavLink} to="/:userId/cart" exact disabled>Cart</Nav.Link>
		        	</Fragment>

		        }
		        {
	        		user.id !== null && user.isAdmin === true ?
		        	<Fragment>
	        			<Nav.Link as={NavLink} to="/create" exact>Add Products</Nav.Link>
		        	</Fragment>
	        		:
	        			<Nav.Link as={NavLink} to="/create" exact hidden>Add Products</Nav.Link>
		        }
		      </Nav>
		      <Nav>
		      { user.id !== null ?
		        	<Fragment>
		        	<Nav.Link className="btn btn-primary" as={NavLink} to="/logout" exact>Logout</Nav.Link>
		        	</Fragment>
		        	:
		        	<Fragment>
				        	<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
				        	<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
		        	</Fragment>
		        }
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
		/*<Navbar bg="primary" expand="lg" className="mb-5">
		  <Container>
		    <Navbar.Brand as={Link} to="/">BS</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
		        <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
		        {
	        		user.id !== null && user.isAdmin !== true ?
		        	<Nav.Link as={NavLink} to="/wishlist" exact>Wishlist</Nav.Link>
		        	:
		        	<Nav.Link as={NavLink} to="/wishlist" exact disabled>Wishlist</Nav.Link>

		        }
		        {
	        		user.id !== null && user.isAdmin === true ?
		        	<Fragment>
		        		<Nav.Link as={NavLink} to="/:userId/orders" exact disabled>Cart</Nav.Link>
	        			<Nav.Link as={NavLink} to="/create" exact>Add Products</Nav.Link>
		        	</Fragment>
	        		:
		        	<Nav.Link as={NavLink} to="/:userId/orders" exact>Cart</Nav.Link>
		        }
		        { user.id !== null ?
		        	<Fragment>
		        	<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
		        	</Fragment>
		        	:
		        	<Fragment>
				        	<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
				        	<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
		        	</Fragment>
		        }
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	*/
	)
};