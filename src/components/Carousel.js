// import the necessary components
import {Row, Col, Card, Carousel, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCarousel(){
	return(
		<Container className="mb-5 containerCarousel">
		<Row className="productCarousel mt-3 mb-3">
			<Col xs={12}>
			<Container className='Carousel'>

			<h2 className="carouselText">Featuring our best sellers!</h2>
				<Carousel >
				  <Carousel.Item>
				  <Link to="/products/61b9475e48963fdd97eba90f">
				    <img
				      className="d-block w-100 carouselImg"
				      src="https://media.istockphoto.com/photos/worried-east-asian-woman-checking-her-face-skin-in-the-mirror-picture-id1270369920?b=1&k=20&m=1270369920&s=170667a&w=0&h=Z8837YIJW6nN27IzB5ntqnBai467WM8M4sg3O06gRfo="
				      alt="First slide"
				    />
				  </Link>
				    <Carousel.Caption >
				      <h3 className="carouselText">Bluetooth Mirror</h3>
				    </Carousel.Caption>
				  </Carousel.Item>
				  <Carousel.Item>
				  <Link to="/products/61b6a983e7c90a71b0f6cffa">
					    <img
					      className="d-block w-100 carouselImg"
					      src="https://res.cloudinary.com/yourmechanic/image/upload/dpr_auto,f_auto,q_auto/v1/article_images/SBF_windshield_wiper_blade"
					      alt="Second slide"
					    />
				  </Link>

				    <Carousel.Caption>
				      <h3 className="carouselText">Bluetooth Eyeglass Wiper</h3>
				    </Carousel.Caption>
				  </Carousel.Item>
				</Carousel>
			</Container>
			</Col>
		</Row>
		</Container>
	)
};